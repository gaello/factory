﻿using UnityEngine;

/// <summary>
/// Factory design pattern.
/// </summary>
public class Factory : MonoBehaviour
{
    // Reference to prefab.
    [SerializeField]
    private MonoBehaviour prefab;

    /// <summary>
    /// Creating new instance of prefab.
    /// </summary>
    /// <returns>New instance of prefab.</returns>
    public MonoBehaviour GetNewInstance()
    {
        return Instantiate(prefab);
    }
}
