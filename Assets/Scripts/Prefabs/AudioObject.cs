﻿using UnityEngine;

/// <summary>
/// Audio object.
/// Used as example to create AudioFactory.
/// </summary>
public class AudioObject : MonoBehaviour
{
    [SerializeField]
    private AudioSource audio;
    public AudioSource Audio => audio;
}
