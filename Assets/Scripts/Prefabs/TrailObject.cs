﻿using UnityEngine;

/// <summary>
/// Trail object.
/// Used as example to create TrailFactory.
/// </summary>
public class TrailObject : MonoBehaviour
{
    [SerializeField]
    private TrailRenderer trail;
    public TrailRenderer Trail => trail;
}
