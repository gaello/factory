﻿using UnityEngine;

/// <summary>
/// Simple rotating script
/// </summary>
public class SimpleRotate : MonoBehaviour
{
    // Min rotation speed
    [SerializeField]
    private float minRotationSpeed = -50;
    // Max rotation speed
    [SerializeField]
    private float maxRotationSpeed = 50;

    // Current rotation speed
    private float rotationSpeed = 50;

    /// <summary>
    /// Unity's method called on object enable
    /// </summary>
    private void OnEnable()
    {
        // Get random value for speed between min and max
        rotationSpeed = Random.Range(minRotationSpeed, maxRotationSpeed);
    }

    /// <summary>
    /// Unity's method called every frame
    /// </summary>
    private void Update()
    {
        // Rotate object every frame
        transform.localEulerAngles += Vector3.forward * rotationSpeed * Time.deltaTime;
    }
}
