# Factory Design Pattern for Unity

I assume that you are looking for some information about how to implement Factory design pattern in Unity.

This repository contains example how you can do it! In addition to this repository I also made a post about it that you can find here: https://www.patrykgalach.com/2019/03/28/implementing-factory-design-pattern-in-unity/

Enjoy!

---

# How to use it?

This repository contains an example of how you can control UI in Unity using State Machine.

If you want to see that implementation of it, go straight to [Assets/Scripts/](https://bitbucket.org/gaello/factory/src/master/Assets/Scripts/) folder. You will find all code that I wrote to make it work. Code also have comments so it would make a little bit more sense.

I hope you will enjoy it!

---

#Well done!

You have just learned about implementing Factory design pattern in Unity!

##Congratulations!

For more visit my blog: https://www.patrykgalach.com

